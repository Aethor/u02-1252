#+TITLE: TD : Les fonctions en C++
#+AUTHOR: Arthur Amalvy
#+OPTIONS: num:nil


* Rappel

En C++ le retour de plusieurs valeur avec =return= n'est pas
permis. En C++, on distingue deux types de passage d'argument par
valeur ou par variable (par référence) :

#+begin_src cpp
void somme_produit(int a, int b, int &s, int &p) {
    s = a + b ;
    p = a * b ;
}
#+end_src

Utilisation : 

#+begin_src cpp
int S, P ;
somme_produit(5, 6, S, P) ;
cout << S;
cout << P;
#+end_src


* Exercice 0

** 1

Quelle modification faut-il apporter au programme suivant pour qu'il
devienne correct :

#+begin_src cpp
#include <iostream>
using namespace std;

int main() {
    int p = 5;
    triple(p);
    cout << "le triple de " << p << " est égale à " << n;
    return 0;
}

int triple(int r) {
    int n;
    n = 3 * r;
    return n;
}
#+end_src


@@html:<details>@@
@@html:<summary>Solution</summary>@@

Il faut :

1. Soit déclarer la fonction ~triple~ avec un prototype de fonction
   avant le ~main~, soit déplacer la fonction ~triple~ avant la ~main~
2. Récupérer la valeur retournée par la fonction ~triple~ dans une
   variable n.

#+begin_src cpp :tangle exercice0.1.cpp
#include <iostream>
using namespace std ;

int triple(int r);

int main() {
    int p = 5;
    int n = triple(p);
    cout << "le triple de " << p << " est égale à " << n;
    return 0;
}

int triple(int r) {
    int n;
    n = 3 * r;
    return n;
}
#+end_src

@@html:</details>@@


** 2

Qu'affiche le programme suivant ?

#+begin_src cpp :tangle exercice0.2.cpp
#include <iostream>
using namespace std;

int n = 10, q = 2;

int fct(int);
void f(void);

int main() {
    int n = 0, p = 5;
    n = fct(p);
    cout << "A : dans main, n = " << n << " p = " << p << " q = " << q << "\n";
    f();
}

int fct(int p) {
    int q;
    q = 2 * p + n;
    cout << "B : dans fct, n = " << n << " p = " << p << " q = " << q << "\n";
    return q;
}

void f(void) {
    int p = q * n;
    cout << "C : dans f, n = " << n << " p = " << p << " q = " << q << "\n";
}
#+end_src


@@html:<details>@@
@@html:<summary>Solution</summary>@@

Le programme affichera :

#+begin_example
B : dans fct, n = 10 p = 5 q = 20
A : dans main, n = 20 p = 5 q = 2
C : dans f, n = 10 p = 20 q = 2
#+end_example

@@html:</details>@@


* Exercice 1

** A

Écrire une fonction ~est_premier~ qui détermine si un nombre entier
positif a est premier. La fonction doit retourner un booléen.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
bool premier(int n) {
    for(int i=2;i<n;i++) {
	if(n%i==0) {
	    return false;  
	} 
    } 
    return true;
}
#+end_src
@@html:</details>@@


** B

Ecrire une fonction qui imprime tous les nombres premiers entre une
valeur min et une valeur max. Tester dans un main.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp :tangle exercice1.b.cpp
#include <iostream>
using namespace std;

bool premier(int n) {
    for(int i=2;i<n;i++) {
	if(n%i==0) {
	    return false;  
	} 
    } 
    return true;
}

void imprime_premiers(int n, int m) {
    for(int i = n; i <= m; i++) {
	if(premier(i)) {
	    cout<<i<<endl;  
	} 
    }
}

int main() {
    imprime_premiers(10, 20);
}
#+end_src
@@html:</details>@@


* Exercice 2

** A

Écrire une fonction qui affiche les solutions de l'équation d’une
fonction $ax^2 + bx + c = 0$. Écrire un main permettant de tester la
fonction. Il y a plusieurs cas : 0 solution, 1 solution, 2 solutions
et une infinité de racines.

#+begin_example
si a=0
    si b=0
	si c=0 alors une infinité de solutions
	sinon aucune de solutions
    sinon S = -c/b
sinon
    Delta = b^2 - 4ac
    si (Delta > 0) alors 2 solutions ….
    si (Delta < 0) alors 0 solutions
    si (Delta = 0) alors une solutiion ...
#+end_example

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp :tangle exercice2.a.cpp
#include <iostream>
#include <cmath>
using namespace std;


void afficher_solution(double a, double b, double c) {
    if(a == 0) {
        if(b == 0) {
            if (c == 0) {
		cout << "Il y a une infinité de solutions" << endl;
            } else {
		cout << "Il n'y a aucune solution" << endl;
	    } 
	} else {
	    cout << "Il y'a une solution : " << -c/b << endl;
        }
    } else {
        double Delta = b * b - 4 * a * c;
        if (Delta > 0) {
            double x1 = (-b - sqrt(Delta)) / (2 * a);
            double x2 = (-b + sqrt(Delta)) / (2 * a);
	    cout << "Il y'a deux solutions : " << x1 << " et " << x2 << endl;
        } else if(Delta < 0) {
	    cout << "Il n'y a aucune solution" << endl;
        } else {
	    cout << "Il y a une solution : " << -b / (2 * a) << endl;
        }
    }
}

int main() {
    afficher_solution(-2, 2, 1);
}
#+end_src
@@html:</details>@@


** B

Écrire une fonction « resoudre » qui retourne (qui n'affiche pas) les
solutions de l'équation $ax^2 + bx + c = 0$. La fonction ne doit faire
aucun affichage ni saisie. Elle doit envoyer suffisamment
d'informations pour qu'une fonction d'affichage puisse afficher des
messages tels que :

#+begin_example
Votre équation admet 2 solutions : 2.3 et 6.7
Votre équation admet 1 solution : 4.5
Votre équation n’admet pas de solutions
#+end_example

*** 1

Donner le prototype de la fonction =resoudre=.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
void resoudre(double a, double b, double c, int &nb_solutions, double &x1, double &x2);
#+end_src
@@html:</details>@@


*** 2

Écrire la fonction =resoudre=.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp :tangle exercice2.b.1.cpp
#include <iostream>
#include <cmath>
using namespace std;

// calcule les solutions de l'équation ax^2 + bx + c = 0.
// après appel, nb_solutions vaut le nombre de solutions à l'équation.
// - si le nombre de solutions est infini, nb_solutions est mis à -1.
//   x1 et x2 ne sont pas modifiés.
// - si le nombre de solutions est 0, x1 et x2 ne sont pas modifiés
// - si le nombre de solutions est 1, seul x1 est modifiés
// - si le nombre de solutions est 2, x1 et x2 sont modifiés
void resoudre(double a, double b, double c, int &nb_solutions, double &x1, double &x2) {
    if(a == 0) {
        if(b == 0) {
            if (c == 0) {
		// infinité de solutions : on met le nombre de solutions à -1
		nb_solutions = -1;
            } else {
		// pas de solutions
		nb_solutions = 0;
	    } 
	} else {
	    nb_solutions = 1;
	    x1 = -c / b;
        }
    } else {
        double Delta = b * b - 4 * a * c;
        if (Delta > 0) {
	    nb_solutions = 2;
            x1 = (-b - sqrt(Delta)) / (2 * a);
            x2 = (-b + sqrt(Delta)) / (2 * a);
        } else if(Delta < 0) {
	    // pas de solutions
	    nb_solutions = 0;
        } else {
	    nb_solutions = 1;
	    x1 = -b / (2 * a);
        }
    }
}

int main() {
    int nb_solutions = 0;
    double x1, x2 = 0;
    resoudre(-2, 2, 1, nb_solutions, x1, x2);
}
#+end_src
@@html:</details>@@


*** 3

Écrire une fonction =afficher= qui utilise le retour de la fonction
=resoudre= pour afficher les solutions.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp :tangle exercice2.b.3.cpp
#include <iostream>
#include <cmath>
using namespace std;

// calcule les solutions de l'équation ax^2 + bx + c = 0.
// après appel, nb_solutions vaut le nombre de solutions à l'équation.
// - si le nombre de solutions est infini, nb_solutions est mis à -1.
//   x1 et x2 ne sont pas modifiés.
// - si le nombre de solutions est 0, x1 et x2 ne sont pas modifiés
// - si le nombre de solutions est 1, seul x1 est modifiés
// - si le nombre de solutions est 2, x1 et x2 sont modifiés
void resoudre(double a, double b, double c, int &nb_solutions, double &x1, double &x2) {
    if(a == 0) {
        if(b == 0) {
            if (c == 0) {
                // infinité de solutions : on met le nombre de solutions à -1
                nb_solutions = -1;
            } else {
                // pas de solutions
                nb_solutions = 0;
            }
        } else {
            nb_solutions = 1;
            x1 = -c / b;
        }
    } else {
        double Delta = b * b - 4 * a * c;
        if (Delta > 0) {
            nb_solutions = 2;
            x1 = (-b - sqrt(Delta)) / (2 * a);
            x2 = (-b + sqrt(Delta)) / (2 * a);
        } else if(Delta < 0) {
            // pas de solutions
            nb_solutions = 0;
        } else {
            nb_solutions = 1;
            x1 = -b / (2 * a);
        }
    }
}

void afficher(int nb_solutions, double x1, double x2) {
    if(nb_solutions == -1) {
        cout << "Une infinité de solutions" << endl;
    } else if(nb_solutions == 0) {
        cout << "Aucune solutions" << endl;
    } else if(nb_solutions == 1) {
        cout << "Une seule solution : " << x1 << endl;
    } else {
        cout << "Deux solutions : " << x1 << " et " << x2 << endl;
    }
}

int main() {
    int nb_solutions = 0;
    double x1, x2 = 0;
    resoudre(-2, 2, 1, nb_solutions, x1, x2);
    afficher(nb_solutions, x1, x2);
}
#+end_src
@@html:</details>@@


* Exercice 3

À partir d'une date exprimée comme trois numéros j, m, a (jour, mois,
année), déterminer le jour de la semaine. Voici une méthode :

** A

Écrire une fonction qui reçoit un entier à quatre chiffres et qui
retourne deux entiers : un correspondant aux deux premiers chiffres et
un correspondant aux deux derniers chiffres.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
void couper_en_deux(int ap, int &ns, int &as) {
    ns = ap / 100;
    as = ap % 100;
}
#+end_src
@@html:</details>@@


** B

Soit la structure :

#+begin_src cpp
struct date {
    int j;
    int m;
    int a;
};
#+end_src

Soient $m'$ et $a'$ définis comme suit :

\begin{equation}
m' = \left\{
    \begin{array}{ll}
        m - 2 & \mbox{si } m \geq 3 \\
	m + 10 & \mbox{si } m < 3
    \end{array}
\right.
\end{equation}

\begin{equation}
a' = \left\{
    \begin{array}{ll}
        a & \mbox{si } m \geq 3 \\
	a - 1 & \mbox{si } m < 3
    \end{array}
\right.
\end{equation}

Soit $n_s$ les deux premiers chiffres de $a'$ et $a_s$ les deux
derniers chiffres de $a'$. On peut déterminer le jour de la semaine
correspondant à une date donnée grâce à la formule suivante :

\begin{equation}
f = (j + a_s + \frac{a_s}{4} - 2 n_s + \frac{n_s}{4} + \frac{26 m' - 2}{10})mod7
\end{equation}

(explication du calcul : voir https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week)

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
int num_jour(date &d) {
    int mprime, ns, as, aprime;

    if(d.m >= 3) {
        mprime = d.m - 2;
        aprime = d.a;
    } else {
        mprime = d.m + 10;
        aprime = d.a - 1;
    }

    couper_en_deux(aprime, ns, as);
    int f = (d.j + as + (as / 4) - 2 * ns + (ns / 4) + (26 * mprime - 2) / 10) % 7;
    return f;

}
#+end_src
@@html:</details>@@


** C

Écrire une fonction qui prend en argument un tableau de dates et qui affiche les jours
correspondants aux dates qui y sont stockées.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
void afficher_jours(date T[], int N) {
    for(int i = 0; i < N; i++) {
        int f = num_jour(T[i]);
        switch(f) {
        case(0):
            cout << "dimanche" << endl;
            break;
        case(1):
            cout << "lundi" << endl;
            break;
        case(2):
            cout << "mardi" << endl;
            break;
        case(3):
            cout << "mercredi" << endl;
            break;
        case(4):
            cout << "jeudi" << endl;
            break;
        case(5):
            cout << "vendredi" << endl;
            break;
        case(6):
            cout << "samedi" << endl;
            break;
        }
    }

}
#+end_src
@@html:</details>@@


** D

Écrire la fonction ~bool ant(date &d1, date &d2)~, qui renvoie vrai si
~d1~ est antérieure à ~d2~.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
bool ant(date &d1, date &d2) {
    return d1.a < d2.a
	|| (d1.a == d2.a && d1.m < d2.m)
	|| (d1.a == d2.a && d1.m == d2.m && d1.j < d2.j);
}
#+end_src
@@html:</details>@@


** E

Soit MinMax la fonction qui prend en argument un tableau de dates et
sa taille et qui renvoie deux dates : la plus récente et la plus
ancienne.

*** 1

Peut on renvoyer les deux dates par un return ?

@@html:<details>@@
@@html:<summary>Solution</summary>@@
Non (sauf astuce).
@@html:</details>@@


*** 2

Écrire la fonction MinMax (aucun ~cin~, aucun ~cout~).

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
void MinMax(date tableau_dates[], int N, date &date_min, date &date_max) {
    for (int i = 0; i < N; i++) {
	if (i == 0 || ant(tableau_dates[i], date_min)) {
	    date_min = tableau_dates[i];
	} 
	if (i == 0 || ant(date_max, tableau_dates[i])) {
	    date_max = tableau_dates[i];
	}
    }
}
#+end_src
@@html:</details>@@


*** 3

Tester la fonction dans un ~main~.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp :tangle exercice3.e.3.cpp
#include <iostream>
using namespace std;

struct date {
    int j;
    int m;
    int a;
};

void afficher_date(date &d) {
    cout << "(" << d.j << "," << d.m << "," << d.a << ")" << endl;
}

bool ant(date &d1, date &d2) {
    return d1.a < d2.a
	|| (d1.a == d2.a && d1.m < d2.m)
	|| (d1.a == d2.a && d1.m == d2.m && d1.j < d2.j);
}

void MinMax(date tableau_dates[], int N, date &date_min, date &date_max) {
    for (int i = 0; i < N; i++) {
	if (i == 0 || ant(tableau_dates[i], date_min)) {
	    date_min = tableau_dates[i];
	} 
	if (i == 0 || ant(date_max, tableau_dates[i])) {
	    date_max = tableau_dates[i];
	}
    }
}

int main() {
    date tableau_dates[] = {{26, 10, 2021},
			    {25, 10, 2021},
			    {24, 12, 2021},
			    {1, 1, 1997}};
    int size = sizeof(tableau_dates) / sizeof(date);

    date date_min = {};
    date date_max = {};

    MinMax(tableau_dates, size, date_min, date_max);

    afficher_date(date_min);
    afficher_date(date_max);
}
#+end_src
@@html:</details>@@
