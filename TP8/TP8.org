#+TITLE: TP8
#+AUTHOR: Arthur Amalvy et Timothée Dhaussy
#+OPTIONS: num:nil
#+PROPERTY: header-args:cpp :exports both :results verbatim


* Tri Fusion (Merge Sort)

Principe : Le tri fusion utilise la stratégie dite « diviser pour régner ».

- Etape1 : On divise le tableau en deux sous-tableaux.
- Etape2 : On trie (par récursivité) chaque sous-tableau.
- Etape3 : On fusionne les deux sous-tableaux, pour obtenir un tableau
  complet trié.


Désignons par a et b (avec a≤b) les indices, initialement à 0 et n-1,
indiquant le premier élément de T (ou d’un sous-tableau de T) et le
dernier élément. L’implémentation de ces 3 étapes donne l’algorithme
suivant :

#+begin_example
Algorithme : tri_fusion(T,a,b)
    si a<b alors
	m=[(a+b)/2]
	afficher("appel tri_fusion:","a=",a,"b=",m,T)
	TriFusion(T,a,m)
	afficher("appel tri_fusion:","a=",m+1,"b=",b,T)
	TriFusion(T,m+1,b)
	Fusion(T,a,b,m)
	afficher("fusion:","a=",a,"m=",m,"b=",b,T)
    fin
fin
#+end_example

- a : Exécutez l’algorithme ci-dessus avec ~T = [3,1,0,9,1,2,6,8,3,4,7]~
- b : Écrire en C++ la fonction fusion ~void fusion (int T[],int a,int
  b,int m)~ qui fusionne les éléments partant de l’indice a jusqu’à
  l’indice m (supposés triés entre eux) avec les éléments partant de
  l’indice m+1 à l’indice b (supposés triés entre eux)
- c : Écrire en C++ la fonction récursive permettant de réaliser le
  tri fusion d’un tableau : ~void tri_fusion(int T[], int a, int b)~


** Version avec ~std::vector~

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp

#include <vector>
#include <iostream>
using namespace std;


void afficher_tableau(vector<int> T) {
    for (int i = 0; i < T.size(); i++) {
        cout << T[i] << " ";
    }
    cout << endl;
}

vector<int> fusionner_vecteurs(vector<int> a, vector<int> b) {

    vector<int> out{};

    int i = 0; 
    int j = 0; 

    while (i < a.size() && j < b.size()) {
        if (a[i] < b[j]) {
            out.push_back(a[i]);
            i++;
        } else {
            out.push_back(b[j]);
            j++;

        }
    }

    while (j != b.size() || i != a.size()) {
        if (j < b.size()) {
            out.push_back(b[j]);
            j++;
        }
        if (i < a.size()) {
            out.push_back(a[i]);
            i++;
        }
    }

    return out;
}

vector<int> tri_fusion(vector<int> a) {

    // cas trivial
    if (a.size() <= 1) {
        return (a);
    }

    int milieu = a.size() / 2;

    vector<int> v1;
    vector<int> v2;

    // remplir le 1er vecteur avec la moitié du tableau
    for (int i = 0; i < milieu; i++) {
        v1.push_back(a[i]);
    }

    // remplir le second vecteur avec la moitié du tableau
    for (int j = milieu; j < a.size(); j++) {
        v2.push_back(a[j]);
    }

    v1 = tri_fusion(v1);
    v2 = tri_fusion(v2);
    vector<int> v_fusion = fusionner_vecteurs(v1, v2);

    return v_fusion;
}

int main() {
    vector<int> v{1, 4, 7, 5, 2};
    v = tri_fusion(v);
    afficher_tableau(v);
}
#+end_src

#+RESULTS:
: 1 2 4 5 7
@@html:</details>@@

** Version "classique"

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
#include <iostream>
using namespace std;


void afficher_tableau(int T[], int N) {
    for (int i = 0; i < N; i++) {
        cout << T[i] << " ";
    }
    cout << endl;
}

void fusion(int t[], int debut, int fin) {

    int r[fin - debut + 1];
    int milieu = (debut + fin) / 2;
    int i1 = debut, i2 = milieu + 1, k = 0;

    while (i1 <= milieu && i2 <= fin) {
        if (t[i1] < t[i2]) {
            r[k] = t[i1];
            i1++;
        } else {
            r[k] = t[i2];
            i2++;
        }
        k++;
    }

    while (i1 <= milieu) {
        r[k] = t[i1];
        i1++;
        k++;
    }

    while (i2 <= fin) {
        r[k] = t[i2];
        i2++;
        k++;
    }

    for(k = 0; k <= fin - debut; k++) {
        t[debut + k] = r[k];
    }
}

void tri_fusion(int T[], int debut, int fin) {
    if (debut < fin) {
	int milieu = (debut + fin) / 2;
	tri_fusion(T, debut, milieu);
	tri_fusion(T, milieu + 1, fin);
	fusion(T, debut, fin);
    }
}

int main() {
    int tableau[] = {1, 4, 7, 5, 2};
    tri_fusion(tableau, 0, 4);
    afficher_tableau(tableau, 5);
}
#+end_src

#+RESULTS:
: 1 2 4 5 7

@@html:</details>@@


* Tri Rapide (Quick Sort)

Principe : Soit T un tableau à trier. On se donne un élément
quelconque du tableau (en général le premier à gauche) nommé pivot. On
effectue une partition du tableau consistant à réorganiser les
éléments de T de la façon suivante : T[0...m − 1], T[m], T[m +
1...n-1] T[m] contient le pivot choisi. T[0...m − 1] contient les
éléments de T inférieurs à T[m] et T[m + 1...n-1] ceux
supérieurs. Notez que les valeurs dans T[0...m−1] et T[m+1...n-1] ne
sont pas nécessairement triées. Pour les trier on réitère
récursivement ce qui a été fait sur T sur chacun de ces sous-
tableaux. L’algorithme est alors le suivant :

#+begin_example
Algorithme : TriRapide(T,a,b)
    si a < b alors
	afficher("avant appel partitionnement",a,b,T)
	m=Partitionnement(T,a,b)
	afficher("apres appel partitionnement",a,b,m,T)
	afficher("appel tri_rapide à gauche",a,m-1)
	TriRapide(T,a,m-1)
	afficher("appel tri_rapide à droite ",m+1,b)
	TriRapide(T,m+1,b)
    fin
fin
#+end_example

Un algorithme de partitionnement possible est le suivant : On
considère comme pivot le premier élément du tableau en cours de
traitement, T[a]. On utilise deux compteurs l et k initialisés aux
deux extrémités (gauche pour l, et droite pour k) du tableau. Tant que
T[l] ≤ T[a], l est incrémenté. Inversement tant que T[k] > T[a], k est
décrémenté. On échange alors T[l] et T[k] puis on continue de façon
analogue jusqu’à ce que les indices l et k se croisent.

- a : Exécutez l’algorithme pour ~T = [3,5,8,9,1,5,2,2,10]~
- b : Écrire en C++ la fonction ~partitionnement(int T[],int a,int b)~
- c : Écrire en C++ la fonction ~tri_rapide(int T[], int a, int b)~

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
#include <iostream>
using namespace std;


void afficher_tableau(int T[], int N) {
    for (int i = 0; i < N; i++) {
        cout << T[i] << " ";
    }
    cout << endl;
}

void echanger(int tableau[], int a, int b) {
    int temp = tableau[a];
    tableau[a] = tableau[b];
    tableau[b] = temp;
}

int partition(int tableau[], int deb, int fin) {
    int compt = deb;
    int pivot = tableau[deb];
    int i;

    for(i = deb + 1; i <= fin; i++) {
        if(tableau[i] < pivot) {
            compt++;
            echanger(tableau, compt, i);
        }
    }
    echanger(tableau, compt, deb);
    return(compt);
}

void tri_rapide(int tableau[], int debut, int fin) {
    if(debut < fin) {
        int pivot = partition(tableau, debut, fin);
        tri_rapide(tableau, debut, pivot - 1);
        tri_rapide(tableau, pivot + 1, fin);
    }
}

int main() {
    int tableau[5] = {1, 4, 7, 5, 2};
    tri_rapide(tableau, 0, 4);
    afficher_tableau(tableau, 5);
}
#+end_src

#+RESULTS:
: 1 2 4 5 7

@@html:</details>@@
