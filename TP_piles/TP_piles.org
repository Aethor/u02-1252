#+TITLE: TP Piles : Correction
#+AUTHOR: Arthur Amalvy
#+OPTIONS: num:nil


* Exercice 1

Proposer une implémentation d'une pile en utilisant la structure :

#+begin_src cpp
struct Pile {
    int taille;
    int nbe;
    int *T;
};
#+end_src

** A

Écrire les différentes fonctions: ~empiler~, ~depiler~, ~vide~,
~pleine~.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
bool pleine(Pile &p) {
    return p.nbe == p.taille;
}

bool vide(Pile &p) {
    return p.nbe == 0;
}

int depiler(Pile &p) {

    if (vide(p)) {
        cout << "Erreur : pile vide";
        exit(1);
    }

    p.nbe--;
    return p.T[p.nbe];
}

bool empiler(Pile &p, int element) {

    if (pleine(p)) {
        return false;
    }

    p.T[p.nbe] = element;
    p.nbe++;
    return true;
}
#+end_src
@@html:</details>@@

** B

Écrire une fonction qui multiplie par deux la taille d'une pile.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
void mutiplierTaille(Pile &p) {
    int *nouveauT = new int[2 * p.taille];
    for (int i = 0; i < p.nbe; i++) {
        nouveauT[i] = p.T[i];
    }
    delete[] p.T;
    p.T = nouveauT;
    p.taille = p.taille * 2;
}
#+end_src
@@html:</details>@@

* Exercice 2

Écrire une fonction qui vérifie si une expression est bien parenthésée :

#+begin_src cpp
bool bienParenthesee(char *T);
#+end_src

Exemple : =((5+4)*(4+8))= est bien parenthésée, par contre
=((5+3)*4+8))= ne l'est pas.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
bool bienParenthesee(char *T) {

    Pile p;
    p.taille = 10; // taille arbitraire
    p.nbe = 0;
    p.T = new char[p.taille];

    int i = 0;

    while (T[i] != '\0') {

	if (T[i] == '(') {
	    empiler(p, '(');
	} else if (T[i] == ')') {
	    if (vide(p)) {
		delete[] p.T;
		return false;
	    }
	    depiler(p);
	}

    }

    delete[] p.T;
    return vide(p);
}
#+end_src
@@html:</details>@@

* Exercice 3

#+begin_src cpp
struct Case {
    // true si le fromage est présent, false sinon
    bool fromage;
    // true si la case est déjà visitée, false sinon
    bool visite;
}

struct Grille {
    int nbl;
    int nbc;
    Case **tab;
};
#+end_src

Grille est une structure représentant une matrice de cases. 

** A

Écrire une fonction de création d'une grille :

#+begin_src cpp
Grille *creer(int nbl, int nbc);
#+end_src

Cette fonction doit allouer le tableau à deux dimensions (~tab~) et
remplir selon un pourcentage donné (par vrai ou faux) les cases,
indiquant si il y a à manger ou pas.

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
Grille *creer(int nbl, int nbc) {

    Grille *grille = new Grille;

    grille->nbl = nbl;
    grille->nbc = nbc;

    grille->tab = new Case*[nbl];
    for (int i = 0; i < nbl; i++) {
        grille->tab[i] = new Case[nbc];
        for (int j = 0; j < nbc; j++) {
            // pas de fromage, case non visitée
            Case c = {false, false};
            grille->tab[i][j] = c;
        }
    }

    return grille;
}
#+end_src
@@html:</details>@@

** B

Écrire une fonction qui renvoie vrai ou faux selon si elle trouve un =
fromage=. La fonction doit aussi renvoyer le chemin permettant d’y
aller. La position initiale est (0,0). Utiliser l’algorithme suivant :

#+begin_src text
chercher(Grille &G) {
    Pile P ; //vide au début
    empiler(P,(0,0)) ;
    tant que (P n’est pas vide) {
        c=depiler(P) ;
        si ( c contient le fromage) alors retourner (vrai, P)
        marquer c comme explorée
        si (il y a encore des POSSIBILITÉS NON EXPLORÉES partant de c) {
            empiler(P,c)
            choisir une possibilité non explorée c’
            empiler(c’)
        }
    }
    retourner(faux,P)
}
#+end_src

@@html:<details>@@
@@html:<summary>Solution</summary>@@
#+begin_src cpp
struct Couple {
    int x;
    int y;
};

bool coordonneeEstValide(Grille* G, Couple coordonnee) {
    return (coordonnee.x >= 0 and coordonnee.x < G->nbc and coordonnee.y >= 0 and coordonnee.y < G->nbl);
}

// récupère la 1ere possibilité qui n'est pas déjà fouillée
// retourne {-1, -1} si plus de possibilité
Couple recupererPossibilite(Grille* G, Couple caseActuelle) {
    // on fouille l'espace autour de la caseActuelle :
    //   -1,-1  +0,-1 +1,-1 
    //   -1,+0    c   +1,+0
    //   -1,+1  +0,+1 +1,+1
    for (int i == -1; i < 2; i++) {
	for (int j == -1; j < 2; j++) {
	    Couple coordonneesPossibilite = {caseActuelle.x + i, caseActuelle.y + j};
	    if (coordonneeEstValide(G, coordonneesPossibilite)	// coordonnée valide
		and not G->tab[i][j].visite			// case non explorée
		)
	    {
		return coordonneesPossibilite;
	    }
	}
    }

    // pas de possibilité : on renvoie une coordonnée invalide
    Couple retour = {-1, -1};
    return retour;
}

Pile chercher(Grille &G) {

    Pile p;

    Couple depart = {0, 0};
    empiler(p, depart); 

    while (not vide(p)) {

        caseActuelle = depiler(p);
        if (G.tab[caseActuelle.x][caseActuelle.y].fromage) {
            // -- on a trouvé le fromage !
            //    la pile forme le chemin vers le fromage
            return p;
        }

        // on marque la case comme déjà explorée
        G->tab[caseActuelle.x][caseActuelle.y].visite = true;

        // on récupère une case voisine
        prochaineCoordonnee = recupererPossibilite(G, caseActuelle);

        // la case voisine est valide : on s'en occupe à la prochaine
        // itération (sinon, on récuperera une autre case dans la pile
        // au début de la boucle)
        if (coordonneeEstValide(G, prochaineCoordonnee)) {
            empiler(p, caseActuelle);
            empiler(p, prochaineCoordonnee);
        }
    }

    // la pile est vide: on a pas trouvé de chemin vers le fromage
    return p;
}
#+end_src
@@html:</details>@@
