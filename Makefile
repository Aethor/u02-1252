targets = $(patsubst %.org,%.html,$(shell find . -type f -name '*.org'))

all: $(targets)

.PHONY: clean
clean:
	rm $(targets)

$(targets): %.html: %.org
	emacs --batch --eval '(progn (find-file "$<") (org-html-export-to-html))'
